function deleteCategory(id) {
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Cancel",
        // closeOnConfirm: false,
        // closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                type: "delete",
                url: 'delete-category/'+id,
                dataType: "json",
                data: {
                    "_token": csrf.token,
                },
                success: function(result, textStatus, jqXHR) {
                    if(result.status == 1) {
                        $('#cat_row_'+id).remove();
                        $('#message').text('Successfully deleted!').css('color', 'green');
                    } else {
                        $('#message').text('Something went wrong!').css('color', 'red');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#message').text('Something went wrong!').css('color', 'red');
                }
            });
        }
    });
}

function deleteSubcategory(id) {
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Cancel",
        // closeOnConfirm: false,
        // closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                type: "delete",
                url: 'delete-subcategory/'+id,
                dataType: "json",
                data: {
                    "_token": csrf.token,
                },
                success: function(result, textStatus, jqXHR) {
                    if(result.status == 1) {
                        $('#row_subcat_'+id).remove();
                        $('#message').text('Successfully deleted!').css('color', 'green');
                    } else {
                        $('#message').text('Something went wrong!').css('color', 'red');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#message').text('Something went wrong!').css('color', 'red');
                }
            });
        }
    });
}

function orderStatusChange(sel, id)
{
    var status = sel.value;
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, "+status+" it!",
        cancelButtonText: "Cancel",
        // closeOnConfirm: false,
        // closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: 'update-order-status/'+id,
                dataType: "json",
                data: {
                    "_token": csrf.token,
                    'order_status': status
                },
                success: function(result, textStatus, jqXHR) {
                    if(result.status == 1) {
                        $('#message').text('Successfully updated!').css('color', 'green');
                    } else {
                        $('#message').text('Something went wrong!').css('color', 'red');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#message').text('Something went wrong!').css('color', 'red');
                }
            });
        }
    });
}

function userVerify(sel, id)
{
    var status = sel.value;
    if (status == 0) {
        var text = 'Declined';
    }
    if (status == 1) {
        var text = 'Verified';
    }
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, "+text+" it!",
        cancelButtonText: "Cancel",
        // closeOnConfirm: false,
        // closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: 'update-user-status/'+id,
                dataType: "json",
                data: {
                    "_token": csrf.token,
                    'verified': status
                },
                success: function(result, textStatus, jqXHR) {
                    if(result.status == 1) {
                        $('#message').text('Successfully updated!').css('color', 'green');
                    } else {
                        $('#message').text('Something went wrong!').css('color', 'red');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#message').text('Something went wrong!').css('color', 'red');
                }
            });
        }
    });
}