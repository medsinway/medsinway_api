<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $primaryKey = 'subcategory_id';

    public  function category()
    {
        return $this->hasOne('App\Category', 'category_id', 'category_id');
    }
}
