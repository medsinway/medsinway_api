<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Subcategory;
use App\Order;
use App\Banner;
use App\User;
use App\Role;
use App\Setting;
use App\Home_page;
use Image;
use \Illuminate\Http\Response;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->fileUploadPath = env("FILE_UPLOAD_PATH", "");
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		return view('home');
	}

	public function users()
	{
		$data['users'] = User::with('role')->orderBy('id', 'DESC')->get();
		return view('users.users', $data);
	}

	public function addUser()
	{
		$data['roles'] = Role::get();
		return view('users.add', $data);
	}

	public function storeUser(Request $request)
	{
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone_number = $request->phone_number;
		$user->password = bcrypt($request->password);
		$user->role_id = $request->role;
		$user->save();
		if ($user) {
			return redirect('/users')->with('message', 'Successfully created!');
		} else {
			return redirect()->back()->with('message', 'Something went wrong');
		}
	}

	public function editUser($id)
	{
		$id = decrypt($id);
		$data['roles'] = Role::get();
		$data['user'] = User::where('id', $id)->first();
		return view('users.add', $data);
	}

	public function updateUserStatus(Request $request, $id)
	{
		$user = User::findOrFail($id);
		$user->verified = $request->verified;
		$user->save();
		return response()->json(['status' => 1]);
	}

	public function updateUser(Request $request, $id)
	{
		$user = User::findOrFail($id);
		$user->name = $request->name;
		$user->email = $request->email;
		if ($request->password != null) {
			$user->password = bcrypt($request->password);
		}
		$user->role_id = $request->role;
		$user->save();
		if ($user) {
			return redirect('/users')->with('message', 'Successfully updated!');
		} else {
			return redirect()->back()->with('message', 'Something went wrong');
		}
	}

	public function getCategories()
	{
		$data['categories'] = Category::orderBy('category_id', 'DESC')->get();
		return view('category.categories', $data);
	}

	public function addCategory()
	{
		return view('category.add-category');
	}

	public function storeCategory(Request $request)
	{
		$cat = new Category();
		$cat->category_name = $request->category_name;
		$cat->category_description = $request->category_description;

		if($file = $request->hasFile('category_image')) {
			$file = $request->category_image;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			// $destinationPath = public_path().'/profile_photos/';
			$destinationPath = $this->fileUploadPath.'/category/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			$image_resize->save($this->fileUploadPath.'/category/thumbnails/' .$fileName);

			$file->move($destinationPath,$fileName);
			// $user_detail->category_image = $fileName;
			$cat->category_image = $fileName;
			$cat->category_image_path = env('APP_URL').'/category/thumbnails/'.$fileName;
		}

		$cat->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function deleteCategory($id)
	{
		$category = Category::find($id);
		$category->delete();
		return response()->json(['status' => 1]);
	}

	public function getSubcategories()
	{
		$data['subcategories'] = Subcategory::orderBy('subcategory_id', 'DESC')->get();
		return view('subcategory.subcategories', $data);
	}

	public function addSubcategory()
	{
		$data['categories'] = Category::orderBy('category_id', 'DESC')->get();
		return view('subcategory.add-subcategory', $data);
	}

	public function storeSubcategory(Request $request)
	{
		$subcat = new Subcategory();
		$subcat->category_id = $request->category_id;
		$subcat->subcategory_name = $request->subcategory_name;
		$subcat->subcategory_description = $request->subcategory_description;

		if($file = $request->hasFile('subcategory_image')) {
			$file = $request->subcategory_image;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			// $destinationPath = public_path().'/profile_photos/';
			$destinationPath = $this->fileUploadPath.'/subcategory/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			$image_resize->save($this->fileUploadPath.'/subcategory/thumbnails/' .$fileName);

			$file->move($destinationPath,$fileName);
			// $user_detail->subcategory_image = $fileName;
			$subcat->subcategory_image = $fileName;
			$subcat->subcategory_image_path = env('APP_URL').'/subcategory/thumbnails/'.$fileName;
		}

		$subcat->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function deleteSubcategory($id)
	{
		$category = Subcategory::find($id);
		$category->delete();
		return response()->json(['status' => 1]);
	}

	public function getOrders()
	{
		$data['orders'] = Order::orderBy('order_id', 'DESC')->get();
		return view('order.orders', $data);
	}

	public function orderNow()
	{
		return view('order.order_now');
	}

	public function storeOrderNow(Request $request)
	{
		$order = new Order();
		$order->order_name = $request->order_name;
		$order->phone_number = $request->phone_number;
		$order->pincode = $request->pincode;
		$order->address = $request->address;
		$order->name = $request->name;
		$order->email = $request->email;
        $order->age = $request->age;
        $order->gender = $request->gender;
        $order->note = $request->note;
        $order->creator_name = $request->creator_name;
        $order->creator_phone = $request->creator_phone;
        $order->creator_email = $request->creator_email;
        $order->creator_type = $request->creator_type;
		$order->prescription_description = $request->prescription_description;
		$order->expected_delivery = $request->expected_delivery;

		if($file = $request->hasFile('prescription_image')) {
			$file = $request->prescription_image;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			// $destinationPath = public_path().'/profile_photos/';
			$destinationPath = $this->fileUploadPath.'/prescription_image/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			// $image_resize->save($this->fileUploadPath.'/prescription_image/thumbnails/' .$fileName);

			$file->move($destinationPath,$fileName);
			$order->prescription_image = $fileName;
		}


        if($request->hasfile('order_image'))
		{
			foreach($request->file('order_image') as $file)
			{
				$fileName = $file->getClientOriginalName();
				$fileName = rand(111,99999).'_'.$fileName;
				// $destinationPath = public_path().'/profile_photos/';
				$destinationPath = $this->fileUploadPath.'/order/';

				$image_resize = Image::make($file->getRealPath());
				$image_resize->resize(100, 100);

				$file->move($destinationPath,$fileName);
 
				$data[] = $fileName;
			}

			$order->order_image = json_encode($data);
		}

		// if($file = $request->hasFile('order_image')) {
		// 	$file = $request->order_image;
		// 	$fileName = $file->getClientOriginalName();
		// 	$fileName = rand(111,99999).'_'.$fileName;
		// 	// $destinationPath = public_path().'/profile_photos/';
		// 	$destinationPath = $this->fileUploadPath.'/order/';

		// 	$image_resize = Image::make($file->getRealPath());
		// 	$image_resize->resize(100, 100);

		// 	// $image_resize->save($this->fileUploadPath.'/order/thumbnails/' .$fileName);

		// 	$file->move($destinationPath,$fileName);
		// 	$order->order_image = $fileName;
		// }

		$order->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function editOrderNow($id)
	{
		$id = decrypt($id);
		$data['order'] = Order::where('order_id', $id)->first();
		return view('order.order_now', $data);
	}

	public function updateOrderStatus(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$order->order_status = $request->order_status;
		$order->save();
		return response()->json(['status' => 1]);
	}

	public function updateOrderNow(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$order->order_name = $request->order_name;
		$order->phone_number = $request->phone_number;
		$order->pincode = $request->pincode;
		$order->address = $request->address;
		$order->name = $request->name;
		$order->email = $request->email;
        $order->age = $request->age;
        $order->gender = $request->gender;
        $order->note = $request->note;
        $order->creator_name = $request->creator_name;
        $order->creator_phone = $request->creator_phone;
        $order->creator_email = $request->creator_email;
        $order->creator_type = $request->creator_type;
		$order->prescription_description = $request->prescription_description;
		$order->expected_delivery = $request->expected_delivery;

		if($file = $request->hasFile('prescription_image')) {
			$file = $request->prescription_image;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			// $destinationPath = public_path().'/profile_photos/';
			$destinationPath = $this->fileUploadPath.'/prescription_image/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			// $image_resize->save($this->fileUploadPath.'/prescription_image/thumbnails/' .$fileName);

			$file->move($destinationPath,$fileName);
			$order->prescription_image = $fileName;
		}

		if($request->hasfile('order_image'))
        {
            foreach($request->file('order_image') as $file)
            {
                $fileName = $file->getClientOriginalName();
                $fileName = rand(111,99999).'_'.$fileName;
                // $destinationPath = public_path().'/profile_photos/';
                $destinationPath = $this->fileUploadPath.'/order/';

                $image_resize = Image::make($file->getRealPath());
                $image_resize->resize(100, 100);

                $file->move($destinationPath,$fileName);
 
                $data[] = $fileName;
            }

            $order->order_image = json_encode($data);
        }

		// if($file = $request->hasFile('order_image')) {
		// 	$file = $request->order_image;
		// 	$fileName = $file->getClientOriginalName();
		// 	$fileName = rand(111,99999).'_'.$fileName;
		// 	// $destinationPath = public_path().'/profile_photos/';
		// 	$destinationPath = $this->fileUploadPath.'/order/';

		// 	$image_resize = Image::make($file->getRealPath());
		// 	$image_resize->resize(100, 100);

		// 	// $image_resize->save($this->fileUploadPath.'/order/thumbnails/' .$fileName);

		// 	$file->move($destinationPath,$fileName);
		// 	$order->order_image = $fileName;
		// }

		$order->order_amount = $request->order_amount;
		$order->actual_amount = $request->actual_amount;
		$order->selling_price = $request->selling_price;
		$order->offer_percent = $request->offer_percent;
		$order->save();
		return redirect()->back()->with('message', 'Successfully updated!');
	}

	public function getHomeDatas()
	{
		$type = request()->type;
		if($type == 'promotions') {
			$data['homes'] = Home_page::where('home_page_type', $type)->orderBy('home_page_id', 'DESC')->get();
		}
		if($type == 'offers') {
			$data['homes'] = Home_page::where('home_page_type', $type)->orderBy('home_page_id', 'DESC')->get();
		}
		if($type == 'deals') {
			$data['homes'] = Home_page::where('home_page_type', $type)->orderBy('home_page_id', 'DESC')->get();
		}

		if(request()->has('type') == false) {
			$data['homes'] = Home_page::orderBy('home_page_id', 'DESC')->get();
		}

		return view('home_pages.home_page', $data);
	}

	public function addHomePages()
	{
		return view('home_pages.add-home_page');
	}

	public function storeHomePages(Request $request)
	{
		$home = new Home_page();
		$home->home_page_heading = $request->home_page_heading;
		$home->home_page_details = $request->home_page_details;
		$home->home_page_starts_at = $request->home_page_starts_at;
		$home->home_page_ends_at = $request->home_page_ends_at;
		$home->home_page_terms_and_conditions = $request->home_page_terms_and_conditions;
		$home->home_page_link = $request->home_page_link;
		$home->home_page_type = $request->home_page_type;

		if($file = $request->hasFile('home_page_image')) {
			$file = $request->home_page_image;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			// $destinationPath = public_path().'/profile_photos/';
			$destinationPath = $this->fileUploadPath.'/home_page_image/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			$image_resize->save($this->fileUploadPath.'/home_page_image/thumbnails/' .$fileName);

			$file->move($destinationPath,$fileName);
			// $user_detail->home_page_image = $fileName;
			$home->home_page_image = $fileName;
			// $home->home_page_image_path = env('APP_URL').'/home_page_image/thumbnails/'.$fileName;
		}

		$home->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function editHomePages(Request $request, $id)
	{
		$id = decrypt($id);
		$data['home'] = Home_page::where('home_page_id', $id)->first();
		return view('home_pages.add-home_page', $data);
	}

	public function updateHomePages(Request $request, $id)
	{
		$home = Home_page::findOrFail($id);
		$home->home_page_heading = $request->home_page_heading;
		$home->home_page_details = $request->home_page_details;
		$home->home_page_starts_at = $request->home_page_starts_at;
		$home->home_page_ends_at = $request->home_page_ends_at;
		$home->home_page_terms_and_conditions = $request->home_page_terms_and_conditions;
		$home->home_page_link = $request->home_page_link;
		$home->home_page_type = $request->home_page_type;

		if($file = $request->hasFile('home_page_image')) {
			$file = $request->home_page_image;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			// $destinationPath = public_path().'/profile_photos/';
			$destinationPath = $this->fileUploadPath.'/home_page_image/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			$image_resize->save($this->fileUploadPath.'/home_page_image/thumbnails/' .$fileName);

			$file->move($destinationPath,$fileName);
			// $user_detail->home_page_image = $fileName;
			$home->home_page_image = $fileName;
			// $home->home_page_image_path = env('APP_URL').'/home_page_image/thumbnails/'.$fileName;
		}

		$home->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function getBanners()
	{
		$data['banners'] = Banner::orderBy('bannerId', 'DESC')->get();
		return view('banner', $data);
	}

	public function addBanner()
	{
		return view('add-banner');
	}

	public function storeBanner(Request $request)
	{
		$banner = new Banner();
		$banner->bannerHeading = $request->bannerHeading;
		$banner->bannerDetails = $request->bannerDetails;
		$banner->bannerType = $request->bannerType;
		$banner->bannerTermsAndConditions = $request->bannerTermsAndConditions;

		if($file = $request->hasFile('bannerImage')) {
			$file = $request->bannerImage;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			$destinationPath = $this->fileUploadPath.'/banners/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			$file->move($destinationPath,$fileName);
			// $user_detail->bannerImage = $fileName;
			$banner->bannerImage = $fileName;
		}

		$banner->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function editBanner($id)
	{
		$id = decrypt($id);
		$data['banner'] = Banner::where('bannerId', $id)->first();
		return view('add-banner', $data);
	}

	public function updateBanner(Request $request, $id)
	{
		$banner = Banner::findOrFail($id);
		$banner->bannerHeading = $request->bannerHeading;
		$banner->bannerDetails = $request->bannerDetails;
		$banner->bannerType = $request->bannerType;
		$banner->bannerTermsAndConditions = $request->bannerTermsAndConditions;

		if($file = $request->hasFile('bannerImage')) {
			$file = $request->bannerImage;
			$fileName = $file->getClientOriginalName();
			$fileName = rand(111,99999).'_'.$fileName;
			$destinationPath = $this->fileUploadPath.'/banners/';

			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(100, 100);

			$file->move($destinationPath,$fileName);
			// $user_detail->bannerImage = $fileName;
			$banner->bannerImage = $fileName;
		}

		$banner->save();
		return redirect()->back()->with('message', 'Successfully updated!');
	}

	public function getSettings()
	{
		$data['settings'] = Setting::get();
		return view('settings', $data);
	}

	public function addSettings()
	{
		return view('add-settings');
	}

	public function storeSettings(Request $request)
	{
		$sett = new Setting;
		$sett->setting_name = $request->setting_name;
		$sett->setting_value = $request->setting_value;
		$sett->save();
		return redirect()->back()->with('message', 'Successfully created!');
	}

	public function editSettings($id)
	{
		$id = decrypt($id);
		$data['setting'] = Setting::find($id);
		return view('add-settings', $data);
	}

	public function updateSettings(Request $request, $id)
	{
		$sett = Setting::findOrFail($id);
		$sett->setting_name = $request->setting_name;
		$sett->setting_value = $request->setting_value;
		$sett->save();
		return redirect()->back()->with('message', 'Successfully updated!');
	}
}
