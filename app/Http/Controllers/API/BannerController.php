<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Subcategory;
use App\Banner;
use Image;
use \Illuminate\Http\Response;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->fileUploadPath = env("FILE_UPLOAD_PATH", "");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Banner::orderBy('banner_id', 'DESC')->get();
        $data->map(function ($file) {
            $file->banner_image_path = env('APP_URL').'/banners/';
            return $file;
        });
        return response()->json([
            'status' => '1',
            'data' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = new Banner();
        $banner->banner_heading = $request->banner_heading;

        if($file = $request->hasFile('banner_image')) {
            $file = $request->banner_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            $destinationPath = $this->fileUploadPath.'/banners/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $file->move($destinationPath,$fileName);
            // $user_detail->banner_image = $fileName;
            $banner->banner_image = $fileName;
        }

        $banner->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $banner]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();
        return response()->json(['status' => '1', 'message' =>'success']);
    }
}
