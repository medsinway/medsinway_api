<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Str;

class UserController extends Controller
{
	/**
	 * Create user
	 *
	 * @param  [string] name
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [string] password_confirmation
	 * @return [string] message
	 */
	public function signup(Request $request)
	{
		$request->validate([
			'name' => 'required|string',
			'email' => 'required|string|email|unique:users',
			'phone_number' => 'required|unique:users',
			'password' => 'required|string',
			'role_id' => 'required',
		]);
		$user = new User([
			'name' => $request->name,
			'email' => $request->email,
			'phone_number' => $request->phone_number,
			'password' => bcrypt($request->password),
			'role_id' => $request->role_id,
			'license_id' => $request->license_id
		]);
		$user->save();
		return response()->json([
			'message' => 'Successfully created user!',
			'user' => $user
		], 200);
	}
  
	/**
	 * Login user and create token
	 *
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [boolean] remember_me
	 * @return [string] access_token
	 * @return [string] token_type
	 * @return [string] expires_at
	 */
	public function login(Request $request)
	{
		$request->validate([
			'email' => 'required|string',
			'password' => 'required|string',
			'remember_me' => 'boolean'
		]);
		// This is for only email and password login
		// $credentials = request(['email', 'password']);
		// if(!Auth::attempt($credentials))
		// 	return response()->json([
		// 		'message' => 'Unauthorized'
		// 	], 401);

		$credentials = $user = User::where('email', $request->email)->orWhere('phone_number', $request->email)->first();
		if ($credentials == null) {
			$credentials = false;
		} else {
			$credentials = true;
		}

		if(!$credentials)
			return response()->json([
				'message' => 'Unauthorized'
			], 401);
		// $user = $request->user(); //This is for only email and passwod login
		$user = User::where('email', $request->email)->orWhere('phone_number', $request->email)->first();

		if ($user->verified == 0) {
			return response()->json([
				'message' => 'User not verified'
			], 200);
		}
		$tokenResult = $user->createToken('Personal Access Token');
		$token = $tokenResult->token;
		if ($request->remember_me)
			$token->expires_at = Carbon::now()->addWeeks(1);
		$token->save();
		return response()->json([
			'access_token' => $tokenResult->accessToken,
			'token_type' => 'Bearer',
			'expires_at' => Carbon::parse(
				$tokenResult->token->expires_at
			)->toDateTimeString(),
			'user' => $user
		]);
	}

	public function forgotPassword(Request $request)
	{
		if ($request->email != null) {
			$checkEmail = User::where('email', $request->email)->first();
			if ($checkEmail != null) {
				$forgot = User::findOrFail($checkEmail->id);
				$new_password = Str::random(10);
				$forgot->password = bcrypt($new_password);
				$forgot->save();
				return response()->json([
					'temp_password' => $new_password
				]);
			}
		}
	}
  
	/**
	 * Logout user (Revoke the token)
	 *
	 * @return [string] message
	 */
	public function logout(Request $request)
	{
		$request->user()->token()->revoke();
		return response()->json([
			'message' => 'Successfully logged out'
		]);
	}
  
	/**
	 * Get the authenticated User
	 *
	 * @return [json] user object
	 */
	public function users(Request $request)
	{
		$users = User::orderBy('id', 'DESC')->get();
		return response()->json(['data' => $users]);
	}

	public function resetPassword(Request $request)
	{
		if ($request->email != null) {
			$checkEmail = User::where('email', $request->email)->first();
			if ($checkEmail != null) {
				$forgot = User::findOrFail($checkEmail->id);
				$forgot->password = bcrypt($request->new_password);
				$forgot->save();
				return response()->json([
					'message' => 'Password reset successfully done'
				]);
			}
		}
	}

	public function profileUpdate(Request $request)
	{
		$request->validate([
			'name' => 'required|string',
			'email' => 'required|string|email|unique:users',
			'phone_number' => 'required|unique:users',
			'password' => 'required|string',
		]);
		$user = new User([
			'name' => $request->name,
			'email' => $request->email,
			'phone_number' => $request->phone_number,
			'password' => bcrypt($request->password),
		]);
		$user->save();
		return response()->json([
			'message' => 'Successfully updated',
			'user' => $user
		], 200);
	}
}