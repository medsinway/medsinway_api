<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Subcategory;
use App\Order;
use Image;
use \Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->fileUploadPath = env("FILE_UPLOAD_PATH", "");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::where('creator_email', Auth::user()->email)
                        ->whereNotNull('creator_email')
                        ->orderBy('order_id', 'DESC')->get();
        $data->map(function ($file) {
            $file->order_image_path = env('APP_URL').'/order/';
            $file->prescription_image_path = env('APP_URL').'/prescription_image/';
            return $file;
        });
        return response()->json([
            'status' => '1',
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        $order->order_name = $request->order_name;
        $order->phone_number = $request->phone_number;
        $order->pincode = $request->pincode;
        $order->address = $request->address;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->age = $request->age;
        $order->gender = $request->gender;
        $order->note = $request->note;
        $order->creator_name = Auth::user()->name;
        $order->creator_phone = Auth::user()->phone_number;
        $order->creator_email = Auth::user()->email;
        $order->creator_type = $request->creator_type;
        $order->prescription_description = $request->prescription_description;

        // $order->order_amount = $request->order_amount;
        // $order->actual_amount = $request->actual_amount;
        // $order->selling_price = $request->selling_price;
        // $order->offer_percent = $request->offer_percent;

        if($file = $request->hasFile('prescription_image')) {
            $file = $request->prescription_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/prescription_image/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            // $image_resize->save($this->fileUploadPath.'/prescription_image/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            $order->prescription_image = $fileName;
        }

        if($request->hasfile('order_image'))
        {
            foreach($request->file('order_image') as $file)
            {
                $fileName = $file->getClientOriginalName();
                $fileName = rand(111,99999).'_'.$fileName;
                // $destinationPath = public_path().'/profile_photos/';
                $destinationPath = $this->fileUploadPath.'/order/';

                $image_resize = Image::make($file->getRealPath());
                $image_resize->resize(100, 100);

                $file->move($destinationPath,$fileName);
 
                $data[] = $fileName;
            }

            $order->order_image = json_encode($data);
        }

        // if($file = $request->hasFile('order_image')) {
        //     $file = $request->order_image;
        //     $fileName = $file->getClientOriginalName();
        //     $fileName = rand(111,99999).'_'.$fileName;
        //     // $destinationPath = public_path().'/profile_photos/';
        //     $destinationPath = $this->fileUploadPath.'/order/';

        //     $image_resize = Image::make($file->getRealPath());
        //     $image_resize->resize(100, 100);

        //     // $image_resize->save($this->fileUploadPath.'/order/thumbnails/' .$fileName);

        //     $file->move($destinationPath,$fileName);
        //     $order->order_image = $fileName;
        // }

        $order->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $order]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    public function storExpressOrder(Request $request)
    {
        $order = new Order;
        if ($request->phone_number != null) {
            $order->phone_number = $request->phone_number;
        } else {
            $order->phone_number = 'NA';
        }

        if ($request->order_name != null) {
            $order->order_name = $request->order_name;
        } else {
            $order->order_name = 'NA';
        }

        if ($request->note != null) {
            $order->note = $request->note;
        } else {
            $order->note = 'NA';
        }

        $order->pincode = 'NA';
        $order->address = 'NA';
        $order->order_type = 'Express';
        $order->creator_name = Auth::user()->name;
        $order->creator_phone = Auth::user()->phone_number;
        $order->creator_email = Auth::user()->email;
        $order->creator_type = $request->creator_type;

        if($file = $request->hasFile('order_image')) {
            $file = $request->order_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            $destinationPath = $this->fileUploadPath.'/order/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $file->move($destinationPath,$fileName);
            $order->order_image = $fileName;
        }

        $order->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $order]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->order_name = $request->order_name;
        $order->phone_number = $request->phone_number;
        $order->pincode = $request->pincode;
        $order->address = $request->address;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->age = $request->age;
        $order->gender = $request->gender;
        $order->note = $request->note;
        $order->creator_name = Auth::user()->name;
        $order->creator_phone = Auth::user()->phone_number;
        $order->creator_email = Auth::user()->email;
        $order->creator_type = $request->creator_type;
        $order->prescription_description = $request->prescription_description;

        if($file = $request->hasFile('prescription_image')) {
            $file = $request->prescription_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/prescription_image/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            // $image_resize->save($this->fileUploadPath.'/prescription_image/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            $order->prescription_image = $fileName;
        }

        if($request->hasfile('order_image'))
        {
            foreach($request->file('order_image') as $file)
            {
                $fileName = $file->getClientOriginalName();
                $fileName = rand(111,99999).'_'.$fileName;
                // $destinationPath = public_path().'/profile_photos/';
                $destinationPath = $this->fileUploadPath.'/order/';

                $image_resize = Image::make($file->getRealPath());
                $image_resize->resize(100, 100);

                $file->move($destinationPath,$fileName);
 
                $data[] = $fileName;
            }

            $order->order_image = json_encode($data);
        }

        // if($file = $request->hasFile('order_image')) {
        //     $file = $request->order_image;
        //     $fileName = $file->getClientOriginalName();
        //     $fileName = rand(111,99999).'_'.$fileName;
        //     // $destinationPath = public_path().'/profile_photos/';
        //     $destinationPath = $this->fileUploadPath.'/order/';

        //     $image_resize = Image::make($file->getRealPath());
        //     $image_resize->resize(100, 100);

        //     // $image_resize->save($this->fileUploadPath.'/order/thumbnails/' .$fileName);

        //     $file->move($destinationPath,$fileName);
        //     $order->order_image = $fileName;
        // }

        $order->order_amount = $request->order_amount;
        $order->actual_amount = $request->actual_amount;
        $order->selling_price = $request->selling_price;
        $order->offer_percent = $request->offer_percent;
        $order->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $order]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
