<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Subcategory;
use App\Promotion;
use Image;
use \Illuminate\Http\Response;

class PromotionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->fileUploadPath = env("FILE_UPLOAD_PATH", "");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Promotion::orderBy('promotion_id', 'DESC')->get();
        return response()->json(['status' => '1', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $promo = new Promotion();
        $promo->promo_heading = $request->promo_heading;
        $promo->promo_details = $request->promo_details;

        if($file = $request->hasFile('promo_image')) {
            $file = $request->promo_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/promotion/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $image_resize->save($this->fileUploadPath.'/promotion/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            // $user_detail->promo_image = $fileName;
            $promo->promo_image = $fileName;
            $promo->promo_image_path = env('APP_URL').'/promotion/thumbnails/'.$fileName;
        }

        $promo->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $promo]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promo = Promotion::find($id);
        $promo->delete();
        return response()->json(['status' => '1', 'message' =>'success']);
    }
}
