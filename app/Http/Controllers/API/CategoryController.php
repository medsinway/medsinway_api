<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Subcategory;
use Image;
use \Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->fileUploadPath = env("FILE_UPLOAD_PATH", "");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Category::orderBy('category_id', "DESC")->get();
        return response()->json(['status' => '1', 'data' => $data]);
    }

    public function subCategoryList()
    {
        $data = Subcategory::with('category')->orderBy('subcategory_id', "DESC")->get();
        return response()->json(['status' => '1', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;

        if($file = $request->hasFile('category_image')) {
            $file = $request->category_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/category/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $image_resize->save($this->fileUploadPath.'/category/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            // $user_detail->category_image = $fileName;
            $category->category_image = $fileName;
            $category->category_image_path = env('APP_URL').'/category/thumbnails/'.$fileName;
        }

        $category->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $category]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }

    }

    public function storeSubcategory(Request $request)
    {
        $subcat = new Subcategory();
        $subcat->category_id = $request->category_id;
        $subcat->subcategory_name = $request->subcategory_name;
        $subcat->subcategory_description = $request->subcategory_description;

        if($file = $request->hasFile('subcategory_image')) {
            $file = $request->subcategory_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/subcategory/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $image_resize->save($this->fileUploadPath.'/subcategory/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            // $user_detail->subcategory_image = $fileName;
            $subcat->subcategory_image = $fileName;
            $subcat->subcategory_image_path = env('APP_URL').'/subcategory/thumbnails/'.$fileName;
        }

        $subcat->save();
        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $subcat]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Category::where('category_id', $id)->first();
        return response()->json(['status' => '1', 'message' =>'success', 'data' => $data]);
    }

    public function showSubcategory($id)
    {
        $data = Subcategory::with('category')->where('subcategory_id', $id)->first();
        return response()->json(['status' => '1', 'message' =>'success', 'data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;

        if($file = $request->hasFile('category_image')) {
            $file = $request->category_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/category/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $image_resize->save($this->fileUploadPath.'/category/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            // $user_detail->category_image = $fileName;
            $category->category_image = $fileName;
            $category->category_image_path = env('APP_URL').'/category/thumbnails/'.$fileName;
        }

        $category->save();

        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $category]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    public function updateSubcategory(Request $request, $id)
    {
        $subcat = Subcategory::findOrFail($id);
        $subcat->category_id = $request->category_id;
        $subcat->subcategory_name = $request->subcategory_name;
        $subcat->subcategory_description = $request->subcategory_description;

        if($file = $request->hasFile('subcategory_image')) {
            $file = $request->subcategory_image;
            $fileName = $file->getClientOriginalName();
            $fileName = rand(111,99999).'_'.$fileName;
            // $destinationPath = public_path().'/profile_photos/';
            $destinationPath = $this->fileUploadPath.'/subcategory/';

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(100, 100);

            $image_resize->save($this->fileUploadPath.'/subcategory/thumbnails/' .$fileName);

            $file->move($destinationPath,$fileName);
            // $user_detail->subcategory_image = $fileName;
            $subcat->subcategory_image = $fileName;
            $subcat->subcategory_image_path = env('APP_URL').'/subcategory/thumbnails/'.$fileName;
        }

        $subcat->save();
        try {
            return response()->json(['status' => '1', 'message' =>'success', 'data' => $subcat]);
        } catch (\Exception $e) {
            return response()->json(['status' => '0', 'message' =>'Something went wrong!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
