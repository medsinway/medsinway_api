<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Subcategory;
use App\Promotion;
use App\Deal;
use Image;
use App\Offer;
use \Illuminate\Http\Response;

class OfferController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->fileUploadPath = env("FILE_UPLOAD_PATH", "");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Offer::orderBy('offer_id', 'DESC')->get();
        $data->map(function ($file) {
            $file->offer_image_path = env('APP_URL').'/offers/';
            return $file;
        });
        return response()->json(['status' => '1', 'data' => $data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDeals()
    {
        $data = Deal::orderBy('deals_id', 'DESC')->get();
        $data->map(function ($file) {
            $file->deals_image_path = env('APP_URL').'/deals_image/';
            return $file;
        });
        return response()->json(['status' => '1', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::find($id);
        $offer->delete();
        return response()->json(['status' => '1', 'message' =>'success']);
    }
}
