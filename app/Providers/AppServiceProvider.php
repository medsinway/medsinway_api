<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view)
        {
            $settings = Setting::get();
            // $arrNew = array();
            // foreach($settings AS $sett){
            //     $arrNew['setting_name'] = $sett->setting_name;
            //     $arrNew['setting_value'] = $sett->setting_value;
            // }

            // $encodedSt = json_encode($arrNew);

            $view->with('settings', $settings);
        });
    }
}
