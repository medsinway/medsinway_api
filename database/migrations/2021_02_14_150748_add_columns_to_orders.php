<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('name')->after('order_image')->nullable();
            $table->string('email')->after('name')->nullable();
            $table->string('age')->after('phone_number')->nullable();
            $table->string('gender')->after('age')->nullable();
            $table->string('note')->after('gender')->nullable();
            $table->string('creator_name')->after('note')->nullable();
            $table->string('creator_phone')->after('creator_name')->nullable();
            $table->string('creator_email')->after('creator_phone')->nullable();
            $table->string('creator_type')->after('creator_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
