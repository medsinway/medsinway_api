<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('order_id');
            $table->string('order_name')->nullable();
            $table->string('order_image')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('pincode')->nullable();
            $table->string('address')->nullable();
            $table->string('prescription_image')->nullable();
            $table->string('prescription_description')->nullable();
            $table->string('order_amount')->nullable();
            $table->string('actual_amount')->nullable();
            $table->string('selling_price')->nullable();
            $table->string('offer_percent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
