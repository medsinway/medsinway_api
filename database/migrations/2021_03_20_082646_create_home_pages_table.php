<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_pages', function (Blueprint $table) {
            $table->bigIncrements('home_page_id');
            $table->string('home_page_heading')->nullable();
            $table->longText('home_page_details')->nullable();
            $table->string('home_page_image')->nullable();
            $table->string('home_page_starts_at', 50)->nullable();
            $table->string('home_page_ends_at', 50)->nullable();
            $table->longText('home_page_terms_and_conditions')->nullable();
            $table->string('home_page_link', 50)->nullable();
            $table->string('home_page_type', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_pages');
    }
}
