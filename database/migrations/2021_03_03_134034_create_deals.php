<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->bigIncrements('deals_id');
            $table->string('deals_heading')->nullable();
            $table->string('deals_details')->nullable();
            $table->string('deals_image')->nullable();
            $table->string('deals_starts_at')->nullable();
            $table->string('deals_ends_at')->nullable();
            $table->string('deals_terms_and_conditions')->nullable();
            $table->string('deals_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
