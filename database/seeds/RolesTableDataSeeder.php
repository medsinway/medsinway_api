<?php

use Illuminate\Database\Seeder;

class RolesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roles = [
            ['role_id' => 1, 'role_name' => 'Admin'],
            ['role_id' => 2, 'role_name' => 'Client'],
            ['role_id' => 3, 'role_name' => 'User'],
        ];

        foreach($roles as $role) {
			DB::table('roles')->insert([
				$role,
			]);
		}
    }
}
