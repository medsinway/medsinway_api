<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'HomeController@users')->name('users');
Route::get('/add-user', 'HomeController@addUser')->name('add-user');
Route::post('/store-user', 'HomeController@storeUser')->name('store-user');
Route::get('/edit-user/{id}', 'HomeController@editUser')->name('edit-user');
Route::post('/update-user/{id}', 'HomeController@updateUser')->name('update-user');
Route::post('/update-user-status/{id}', 'HomeController@updateUserStatus')->name('update-user-status');
//Category
Route::get('/categories', 'HomeController@getCategories')->name('categories');
Route::get('/add-category', 'HomeController@addCategory')->name('add-category');
Route::post('/store-category', 'HomeController@storeCategory')->name('store-category');
Route::get('/view-category', 'HomeController@getCategories')->name('view-category');
Route::get('/edit-category', 'HomeController@getCategories')->name('edit-category');
Route::delete('/delete-category/{id}', 'HomeController@deleteCategory')->name('delete-category');

//Subcategory
Route::get('/sub-categories', 'HomeController@getSubcategories')->name('sub-categories');
Route::get('/add-subcategory', 'HomeController@addSubcategory')->name('add-subcategory');
Route::post('/store-subcategory', 'HomeController@storeSubcategory')->name('store-subcategory');
Route::get('/view-subcategory', 'HomeController@getCategories')->name('view-subcategory');
Route::get('/edit-subcategory', 'HomeController@getCategories')->name('edit-subcategory');
Route::delete('/delete-subcategory/{id}', 'HomeController@deleteSubcategory')->name('delete-subcategory');

//Orders
Route::get('/orders', 'HomeController@getOrders')->name('orders');
Route::get('/order-now', 'HomeController@orderNow')->name('order-now');
Route::post('/store-order-now', 'HomeController@storeOrderNow')->name('store-order-now');
Route::get('/edit-order/{id}', 'HomeController@editOrderNow')->name('edit-order');
Route::post('/update-order-now/{id}', 'HomeController@updateOrderNow')->name('update-order-now');
Route::post('/update-order-status/{id}', 'HomeController@updateOrderStatus')->name('update-order-status');

//Offers
Route::get('/home-pages', 'HomeController@getHomeDatas')->name('home-pages');
Route::get('/add-home-pages', 'HomeController@addHomePages')->name('add-home-pages');
Route::post('/store-home-pages', 'HomeController@storeHomePages')->name('store-home-pages');
Route::get('/edit-home-pages/{id}', 'HomeController@editHomePages')->name('edit-home-pages');
Route::post('/update-home-pages/{id}', 'HomeController@updateHomePages')->name('update-home-pages');

//Home page data (banners, promo's)
Route::get('/home-banners', 'HomeController@getBanners')->name('home-banners');
Route::get('/add-banner', 'HomeController@addBanner')->name('add-banner');
Route::post('/store-banner', 'HomeController@storeBanner')->name('store-banner');
Route::get('/edit-banner/{id}', 'HomeController@editBanner')->name('edit-banner');
Route::post('/update-banner/{id}', 'HomeController@updateBanner')->name('update-banner');

//Settings
Route::get('/settings', 'HomeController@getSettings')->name('settings');
Route::get('/add-settings', 'HomeController@addSettings')->name('add-settings');
Route::post('/store-setting', 'HomeController@storeSettings')->name('store-setting');
Route::get('/edit-setting/{id}', 'HomeController@editSettings')->name('edit-setting');
Route::post('/update-setting/{id}', 'HomeController@updateSettings')->name('update-setting');