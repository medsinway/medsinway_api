<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');
Route::post('signup', 'API\UserController@signup');
Route::post('forgot-password', 'API\UserController@forgotPassword');

Route::group([
  'middleware' => 'auth:api'
], function() {
    Route::get('logout', 'API\UserController@logout');
    Route::get('users', 'API\UserController@users');
    Route::post('reset-password', 'API\UserController@resetPassword');
    Route::post('profile-update', 'API\UserController@profileUpdate');
    //Category
    Route::get('categories', 'API\CategoryController@index');
    Route::get('category/{id}', 'API\CategoryController@show');
    Route::post('store-category', 'API\CategoryController@store');
    Route::post('update-category/{id}', 'API\CategoryController@update');
    //Sub category
    Route::get('sub-categories', 'API\CategoryController@subCategoryList');
    Route::get('sub-category/{id}', 'API\CategoryController@showSubcategory');
    Route::post('store-subcategory', 'API\CategoryController@storeSubcategory');
    Route::post('update-subcategory/{id}', 'API\CategoryController@updateSubcategory');
    //Orders
    Route::post('express-order', 'API\OrderController@storExpressOrder');
    Route::get('orders', 'API\OrderController@index');
    Route::post('order', 'API\OrderController@store');
    Route::post('update-order/{id}', 'API\OrderController@update');
    //Banners
    Route::get('banners', 'API\BannerController@index');
    Route::post('store-banner', 'API\BannerController@store');
    Route::get('delete-banner/{id}', 'API\BannerController@destroy');
    //Promotions
    Route::get('promotions', 'API\PromotionController@index');
    Route::post('store-promotion', 'API\PromotionController@store');
    Route::get('delete-promotion/{id}', 'API\PromotionController@destroy');
    //Offers
    Route::get('offers', 'API\OfferController@index');
    Route::get('deals', 'API\OfferController@getDeals');
});

// Page not found response
Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact https://www.trymysolution.com'], 404);
});