@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Subcategory</h1>
				</div><!-- /.col -->
			</div><!-- /.row -->

			@if (session()->has('message'))
				<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>
						{!! session()->get('message') !!}
					</strong>
				</div>
			@endif

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Subcategory</h3>
					</div>
					<!-- /.card-header -->
						<section class="eyevn">
							<form class="form-horizontal" action="{{ route('store-subcategory') }}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="container">
									<div class="vision">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Category Name</label>
													<div class="select2-purple marginbottom">
														<select class="form-control select2" name="category_id">
															@foreach ($categories as $category)
																<option value="{{$category->category_id}}">{{$category->category_name}}</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Subcategory Name</label>
													<input type="text" class="form-control" name="subcategory_name" placeholder="subcategory Name" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Subcategory Image</label>
													<input type="file" class="form-control" name="subcategory_image">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Subcategory Description</label>
													<textarea name="subcategory_description" class="form-control"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="pabtns">
										<ul class="list-group list-group-horizontal list-unstyled">
											<li><a class="btn btn-default float-right pbtn" href="{{ route('categories') }}">Cancel</a></li>
											<li><button type="submit" class="btn btn-info ">Submit</button></li>
										</ul>
									</div>
								</div>
							</form>
						</section>
					<!-- </div> -->
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2();
	});
</script>
@endsection
