@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Sub Category</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if (session()->has('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('message') !!}
                    </strong>
                </div>
            @endif

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('add-subcategory') }}" class="btn btn-primary" style="float: right;">Add Subcategory</a>
                    </div>
                    <div id="message"></div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="patients" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Subcategory Name</th>
                                    <th>Subcategory Image</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subcategories as $subcategory)
                                    <tr id="row_subcat_{{$subcategory->subcategory_id}}">
                                        <td>{{$subcategory->subcategory_id}}</td>
                                        <td>{{$subcategory->subcategory_name}}</td>
                                        <td><img class="img-fluid" src="{{$subcategory->subcategory_image_path}}" alt="Photograph"></td>
                                        <td>{{ $subcategory->created_at }}</td>
                                        <td class="btn-group-vertical btn-group-sm">
                                            <a href="{{ route('view-subcategory', encrypt($subcategory->subcategory_id)) }}" class="btn btn-outline-primary">View</a>
                                            <a href="{{ route('edit-subcategory', encrypt($subcategory->category_id)) }}" class="btn btn-outline-info">Edit</a>
                                            <a href="javascript:void(0);" onclick="deleteSubcategory({{$subcategory->subcategory_id}})" class="btn btn-outline-info">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection
