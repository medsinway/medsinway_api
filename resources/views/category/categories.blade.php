@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Category</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if (session()->has('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('message') !!}
                    </strong>
                </div>
            @endif

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('add-category') }}" class="btn btn-primary" style="float: right;">Add Category</a>
                    </div>
                    <div id="message"></div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="patients" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Category Name</th>
                                    <th>Category Image</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr id="cat_row_{{$category->category_id}}">
                                        <td>{{$category->category_id}}</td>
                                        <td>{{$category->category_name}}</td>
                                        <td><img class="img-fluid" src="{{$category->category_image_path}}" alt="Photograph"></td>
                                        <td>{{ $category->created_at }}</td>
                                        <td class="btn-group-vertical btn-group-sm">
                                            <a href="{{ route('view-category', encrypt($category->category_id)) }}" class="btn btn-outline-primary">View</a>
                                            <a href="{{ route('edit-category', encrypt($category->category_id)) }}" class="btn btn-outline-info">Edit</a>
                                            <a href="javascript:void(0);" onclick="deleteCategory({{$category->category_id}})" class="btn btn-outline-info">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection
