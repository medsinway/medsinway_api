@extends('layouts.app')

@section('content')
<style type="text/css">
	input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Order Now</h1>
				</div><!-- /.col -->
			</div><!-- /.row -->

			@if (session()->has('message'))
				<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>
						{!! session()->get('message') !!}
					</strong>
				</div>
			@endif

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Order Now</h3>
					</div>
					<!-- /.card-header -->
						<section class="eyevn">
							@if (isset($order))
							<form class="form-horizontal" action="{{ route('update-order-now', $order->order_id) }}" method="POST" enctype="multipart/form-data">
							@else
							<form class="form-horizontal" action="{{ route('store-order-now') }}" method="POST" enctype="multipart/form-data">
							@endif
								@csrf
								<div class="container">
									<div class="vision">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Order Name</label>
													<input type="text" class="form-control" name="order_name" placeholder="Order Name" value="@if(isset($order)){{$order->order_name}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Order Image</label>
													<input type="file" class="form-control" name="order_image[]" id="files" multiple @if(isset($order)) @else required @endif>
													@if(isset($order))
													@php $orderImages = json_decode($order->order_image,true); @endphp
													@if(is_array($orderImages) && !empty($orderImages))
													   	<div class="orderImage">
													   @foreach ($orderImages as $orImage)
													    	<img src="{{asset('order/')}}/{{$orImage}}" width="50%" height="20%">
													   @endforeach
													 	</div>
													   @endif
													@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Order Phone</label>
													<input type="text" class="form-control" name="phone_number" placeholder="Phone number" value="@if(isset($order)){{$order->phone_number}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Name</label>
													<input type="text" class="form-control" name="name" placeholder="Your Name" value="@if(isset($order)){{$order->name}}@endif" >
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Email</label>
													<input type="email" class="form-control" name="email" placeholder="Email" value="@if(isset($order)){{$order->email}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Age</label>
													<input type="text" class="form-control" name="age" placeholder="Your Age" value="@if(isset($order)){{$order->age}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Gender</label>
													<select name="gender" class="form-control" >
														<option value="male" @if(isset($order)){{$order->gender == 'male' ? 'selected' : ''}}@endif>Male</option>
														<option value="female" @if(isset($order)){{$order->gender == 'female' ? 'selected' : ''}}@endif>Female</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Pincode</label>
													<input type="text" class="form-control" name="pincode" placeholder="Pincode" value="@if(isset($order)){{$order->pincode}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Address</label>
													<input type="text" class="form-control" name="address" placeholder="Address" value="@if(isset($order)){{$order->address}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Notes</label>
													<input type="text" class="form-control" name="note" placeholder="Notes" value="@if(isset($order)){{$order->note}}@endif" >
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Creator Name</label>
													<input type="text" class="form-control" name="creator_name" placeholder="Doctor Name" value="@if(isset($order)){{$order->creator_name}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Creator Phone</label>
													<input type="text" class="form-control" name="creator_phone" placeholder="Doctor Phone" value="@if(isset($order)){{$order->creator_phone}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Creator Email</label>
													<input type="email" class="form-control" name="creator_email" placeholder="Doctor Phone" value="@if(isset($order)){{$order->creator_email}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Creator Type</label>
													<select name="creator_type" class="form-control">
					                                    <option value="2">Client</option>
					                                    <option value="3">User</option>
					                                    <option value="4">Doctor</option>
					                                    <option value="5">Patient</option>
					                                </select>
												</div>
											</div>
										</div>
										<!-- <div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Prescription Image</label>
													<input type="file" class="form-control" name="prescription_image">
													@if(isset($order))
														<img src="{{asset('prescription_image/')}}/{{$order->prescription_image}}" width="50%" height="30%">
													@endif
												</div>
											</div>
										</div> -->
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Prescription Description</label>
													<textarea name="prescription_description" class="form-control">@if(isset($order)){{$order->prescription_description}}@endif</textarea>
												</div>
											</div>
										</div>
										@if (isset($order))
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Amount</label>
													<input type="text" class="form-control" name="order_amount" placeholder="" value="@if(isset($order)){{$order->order_amount}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Actual Amount</label>
													<input type="text" class="form-control" name="actual_amount" value="@if(isset($order)){{$order->actual_amount}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Selling price</label>
													<input type="text" class="form-control" name="selling_price" value="@if(isset($order)){{$order->selling_price}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">offer percent</label>
													<input type="text" class="form-control" name="offer_percent" value="@if(isset($order)){{$order->offer_percent}}@endif" required="">
												</div>
											</div>
										</div>
										@endif
										<div class="row">
											<div class="col-sm-6">
												<?php
												$setting_val = 0;
													foreach ($settings as $sett) {
														if ($sett->setting_name == 'expected_delivery') {
															$setting_val = $sett->setting_value;
														}
													}
												?>
												<div class="form-group">
													<label for="">Expected Delivery</label>
													<input type="text" class="form-control" name="expected_delivery" value="@if(isset($order)){{$order->expected_delivery}}@else {{$setting_val}}@endif" required="">
												</div>
											</div>
										</div>
									</div>
									<div class="pabtns">
										<ul class="list-group list-group-horizontal list-unstyled">
											<li><a class="btn btn-default float-right pbtn" href="{{ route('orders') }}">Cancel</a></li>
											<li><button type="submit" class="btn btn-info ">Submit</button></li>
										</ul>
									</div>
								</div>
							</form>
						</section>
					<!-- </div> -->
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
</script>
@endsection
