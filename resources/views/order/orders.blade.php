@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Orders</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if (session()->has('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('message') !!}
                    </strong>
                </div>
            @endif

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('order-now') }}" class="btn btn-primary" style="float: right;">Orders now</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="orders" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Order Name</th>
                                    <th>Phone number</th>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Gender</th>
                                    <th>Address</th>
                                    <th>pincode</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>{{$order->order_id}}</td>
                                    <td>{{$order->order_name}}</td>
                                    <td>{{$order->phone_number}}</td>
                                    <td>{{$order->name }}</td>
                                    <td>{{$order->age }}</td>
                                    <td>{{$order->gender }}</td>
                                    <td>{{$order->address }}</td>
                                    <td>{{$order->pincode }}</td>
                                    <td class="btn-group btn-group-sm">
                                        <a href="{{ route('edit-order', encrypt($order->order_id)) }}" class="btn btn-outline-info col-4">View & Edit</a>&nbsp;&nbsp;
                                        <select class="form-control col-8" name="order_status" onchange="orderStatusChange(this, '{{$order->order_id}}')">
                                            <option value="Pending" {{$order->order_status == 'Pending' ? 'selected' : ''}}>Pending</option>
                                            <option value="Accept" {{$order->order_status == 'Accept' ? 'selected' : ''}}>Accept</option>
                                            <option value="Decline" {{$order->order_status == 'Decline' ? 'selected' : ''}}>Decline</option>
                                            <option value="Out_for_delivery" {{$order->order_status == 'Out_for_delivery' ? 'selected' : ''}}>Out for delivery</option>
                                            <option value="delivery" {{$order->order_status == 'delivery' ? 'selected' : ''}}>delivery</option>
                                        </select>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection
