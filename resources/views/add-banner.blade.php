@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Banner</h1>
				</div><!-- /.col -->
			</div><!-- /.row -->

			@if (session()->has('message'))
				<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>
						{!! session()->get('message') !!}
					</strong>
				</div>
			@endif

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Banner</h3>
					</div>
					<!-- /.card-header -->
						<section class="eyevn">
							@if (isset($banner))
							<form class="form-horizontal" action="{{ route('update-banner', $banner->bannerId) }}" method="POST" enctype="multipart/form-data">
							@else
							<form class="form-horizontal" action="{{ route('store-banner') }}" method="POST" enctype="multipart/form-data">
							@endif
								@csrf
								<div class="container">
									<div class="vision">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Banner Heading</label>
													<input type="text" class="form-control" name="bannerHeading" placeholder="banner heading" value="@if(isset($banner)){{$banner->bannerHeading}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Banner Image</label>
													<input type="file" class="form-control" name="bannerImage">
													@if(isset($banner))
														<img width="80" height="80" src="{{asset('banners/')}}/{{$banner->bannerImage}}" alt="banner image">
													@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Banner Details</label>
													<input type="text" class="form-control" name="bannerDetails" placeholder="banner heading" value="@if(isset($banner)){{$banner->bannerDetails}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Banner Type</label>
													<select class="form-control" name="bannerType" required="">
														<option value="homeMainSlider" @if(isset($banner) && $banner->bannerType == 'homeMainSlider') selected @endif>Home Main Slider</option>
														<option value="homeDeals" @if(isset($banner) && $banner->bannerType == 'homeDeals') selected @endif>Home Deals</option>
														<option value="offersPage" @if(isset($banner) && $banner->bannerType == 'offersPage') selected @endif>Offers Page</option>
														<option value="promotionsPage" @if(isset($banner) && $banner->bannerType == 'promotionsPage') selected @endif>Promotions Page</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Banner Terms And Conditions</label>
													<textarea class="form-control" name="bannerTermsAndConditions" required>
														@if(isset($banner)){{$banner->bannerTermsAndConditions}}@endif
													</textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="pabtns">
										<ul class="list-group list-group-horizontal list-unstyled">
											<li><a class="btn btn-default float-right pbtn" href="{{ route('home-banners') }}">Cancel</a></li>
											<li><button type="submit" class="btn btn-info ">Submit</button></li>
										</ul>
									</div>
								</div>
							</form>
						</section>
					<!-- </div> -->
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
</div>
@endsection
