@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Banners</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if (session()->has('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('message') !!}
                    </strong>
                </div>
            @endif

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('add-banner') }}" class="btn btn-primary" style="float: right;">Add Banners</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="patients" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Banner heading</th>
                                    <th>Banner Image</th>
                                    <th>Banner Details</th>
                                    <th>Banner Type</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td>{{$banner->bannerId}}</td>
                                        <td>{{$banner->bannerHeading}}</td>
                                        <td><img width="80" height="80" class="img-fluid" src="{{asset('banners/')}}/{{$banner->bannerImage}}" alt="banner image"></td>
                                        <td>{{$banner->bannerDetails}}</td>
                                        <td>{{$banner->bannerType}}</td>
                                        <td>{{ $banner->created_at }}</td>
                                        <td class="btn-group-vertical btn-group-sm">
                                            <a href="{{ route('edit-banner', encrypt($banner->bannerId)) }}" class="btn btn-outline-info">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection
