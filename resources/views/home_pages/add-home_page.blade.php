@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Home Page</h1>
				</div><!-- /.col -->
			</div><!-- /.row -->

			@if (session()->has('message'))
				<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>
						{!! session()->get('message') !!}
					</strong>
				</div>
			@endif

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Home Page</h3>
					</div>
					<!-- /.card-header -->
						<section class="eyevn">
							@if (isset($home))
							<form class="form-horizontal" action="{{ route('update-home-pages', $home->home_page_id) }}" method="POST" enctype="multipart/form-data">
							@else
							<form class="form-horizontal" action="{{ route('store-home-pages') }}" method="POST" enctype="multipart/form-data">
							@endif
								@csrf
								<div class="container">
									<div class="vision">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Type</label>
													<select name="home_page_type" class="form-control">
														<option value="">Select</option>
														<option value="offers" @if(isset($home) && $home->home_page_type == 'offers') selected @endif>Offer</option>
														<option value="promotions" @if(isset($home) && $home->home_page_type == 'promotions') Selected @endif>Promotion</option>
														<option value="deals" @if(isset($home) && $home->home_page_type == 'deals') Selected @endif>Deal</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Heading</label>
													<input type="text" class="form-control" name="home_page_heading" placeholder="Heading" value="@if(isset($home)){{$home->home_page_heading}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Image</label>
													<input type="file" class="form-control" name="home_page_image" @if(isset($home)) @else required @endif>
													@if(isset($home))
														<img src="{{asset('home_page_image/thumbnails/'.$home->home_page_image)}}" alt="home page image">
													@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Details</label>
													<textarea name="home_page_details" class="form-control" required>@if(isset($home)){{$home->home_page_details}}@endif</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Starts</label>
													<input type="date" class="form-control" name="home_page_starts_at" placeholder="Starts" value="@if(isset($home)){{$home->home_page_starts_at}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Ends</label>
													<input type="date" class="form-control" name="home_page_ends_at" placeholder=" Ends" value="@if(isset($home)){{$home->home_page_ends_at}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Link</label>
													<input type="text" class="form-control" name="home_page_link" placeholder="Link" value="@if(isset($home)){{$home->home_page_link}}@endif" >
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Privacy Policy</label>
													<textarea class="form-control" name="home_page_terms_and_conditions">
														@if(isset($home)){{$home->home_page_terms_and_conditions}}@endif
													</textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="pabtns">
										<ul class="list-group list-group-horizontal list-unstyled">
											<li><a class="btn btn-default float-right pbtn" href="{{ url()->previous() }}">Cancel</a></li>
											<li><button type="submit" class="btn btn-info ">Submit</button></li>
										</ul>
									</div>
								</div>
							</form>
						</section>
					<!-- </div> -->
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
</div>
@endsection
