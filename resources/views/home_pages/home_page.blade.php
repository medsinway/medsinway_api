@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home Page</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if (session()->has('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('message') !!}
                    </strong>
                </div>
            @endif

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('add-home-pages') }}" class="btn btn-primary" style="float: right;">Add</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="patients" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Type</th>
                                    <th>Heading</th>
                                    <th>Details</th>
                                    <th>Image</th>
                                    <th>Starts</th>
                                    <th>Ends</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($homes as $home)
                                    <tr>
                                        <td>{{$home->home_page_id}}</td>
                                        <td>{{$home->home_page_type}}</td>
                                        <td>{{$home->home_page_heading}}</td>
                                        <td>{{$home->home_page_details}}</td>
                                        <td><img class="img-fluid" src="{{asset('home_page_image/thumbnails/'.$home->home_page_image)}}" alt="homes"></td>
                                        <td>{{$home->home_page_starts_at}}</td>
                                        <td>{{$home->home_page_ends_at}}</td>
                                        <td>{{ $home->created_at }}</td>
                                        <td class="btn-group-vertical btn-group-sm">
                                            <a href="{{ route('edit-home-pages', encrypt($home->home_page_id)) }}" class="btn btn-outline-info">View & Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection
