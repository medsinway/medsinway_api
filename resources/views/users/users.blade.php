@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if (session()->has('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('message') !!}
                    </strong>
                </div>
            @endif

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('add-user') }}" class="btn btn-primary" style="float: right;">Add User</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="patients" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    @if ($user->verified == 0)
                                        <?php $backgrndcolor = 'red'; $color = '#fff'; ?>
                                    @endif
                                    @if ($user->verified == 1)
                                        <?php $backgrndcolor = ''; $color = ''; ?>
                                    @endif
                                    <tr style="background-color: {{$backgrndcolor}}; color: {{$color}}">
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone_number}}</td>
                                        <td>{{$user->role->role_name}}</td>
                                        <!-- <td><img width="80" height="80" class="img-fluid" src="{{asset('users/')}}/{{$user->user_image}}" alt="user image"></td> -->
                                        <td class="btn-group btn-group-sm">
                                            <a href="{{ route('edit-user', encrypt($user->id)) }}" class="btn btn-outline-info">Edit</a>&nbsp;&nbsp;&nbsp;
                                            <select class="form-control" name="verified" onchange="userVerify(this, '{{$user->id}}')">
                                                <option value="1" {{$user->verified == '1' ? 'selected' : ''}}>Verified</option>
                                                <option value="0" {{$user->verified == '0' ? 'selected' : ''}}>Declined</option>
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection
