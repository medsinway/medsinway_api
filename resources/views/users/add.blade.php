@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Add User</h1>
				</div><!-- /.col -->
			</div><!-- /.row -->

			@if (session()->has('message'))
				<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>
						{!! session()->get('message') !!}
					</strong>
				</div>
			@endif

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Add User</h3>
					</div>
					<!-- /.card-header -->
						<section class="eyevn">
							@if(isset($user))
							<form class="form-horizontal" action="{{ route('update-user', $user->id) }}" method="POST" enctype="multipart/form-data">
							@else
							<form class="form-horizontal" action="{{ route('store-user') }}" method="POST" enctype="multipart/form-data">
							@endif
								@csrf
								<div class="container">
									<div class="vision">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Name</label>
													<input type="text" class="form-control" name="name" placeholder="Name" required="" value="@if(isset($user)){{$user->name}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Email</label>
													<input type="email" class="form-control" name="email" placeholder="Email" value="@if(isset($user)){{$user->email}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Phone Number</label>
													<input type="number" class="form-control" name="phone_number" placeholder="Phone Number" value="@if(isset($user)){{$user->phone_number}}@endif">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Password</label>
													<input type="password" name="password" class="form-control" placeholder="Password">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Role</label>
													<select name="role" class="form-control">
														@foreach ($roles as $role)
															<?php $select = ''; ?>
															@if(isset($user))
																@if ($role->role_id == $user->role_id)
																<?php $select = 'selected'; ?>
																@else
																<?php $select = ''; ?>
																@endif
															@endif"
															<option value="{{$role->role_id}}" {{$select}}>{{$role->role_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="pabtns">
										<ul class="list-group list-group-horizontal list-unstyled">
											<li><a class="btn btn-default float-right pbtn" href="{{ route('users') }}">Cancel</a></li>
											<li><button type="submit" class="btn btn-info ">@if(isset($user)) Update @else Submit @endif</button></li>
										</ul>
									</div>
								</div>
							</form>
						</section>
					<!-- </div> -->
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
</div>
@endsection
