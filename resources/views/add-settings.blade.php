@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Add Setting</h1>
				</div><!-- /.col -->
			</div><!-- /.row -->

			@if (session()->has('message'))
				<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>
						{!! session()->get('message') !!}
					</strong>
				</div>
			@endif

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Setting</h3>
					</div>
					<!-- /.card-header -->
						<section class="eyevn">
							@if (isset($setting))
							<form class="form-horizontal" action="{{ route('update-setting', $setting->setting_id) }}" method="POST" enctype="multipart/form-data">
							@else
							<form class="form-horizontal" action="{{ route('store-setting') }}" method="POST" enctype="multipart/form-data">
							@endif
								@csrf
								<div class="container">
									<div class="vision">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Seeting Name</label>
													<input type="text" class="form-control" name="setting_name" placeholder="setting name" value="@if(isset($setting)){{$setting->setting_name}}@endif" required="">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Seeting Value</label>
													<input type="text" class="form-control" name="setting_value" placeholder="setting value" value="@if(isset($setting)){{$setting->setting_value}}@endif" required="">
												</div>
											</div>
										</div>
										
									</div>
									<div class="pabtns">
										<ul class="list-group list-group-horizontal list-unstyled">
											<li><a class="btn btn-default float-right pbtn" href="{{ route('settings') }}">Cancel</a></li>
											<li><button type="submit" class="btn btn-info ">Submit</button></li>
										</ul>
									</div>
								</div>
							</form>
						</section>
					<!-- </div> -->
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
</div>
@endsection
